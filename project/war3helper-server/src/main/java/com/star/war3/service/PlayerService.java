package com.star.war3.service;

import com.star.war3.entity.Player;
import com.star.war3.mapper.PlayerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * PlayerService
 *
 * @author ZJY
 * @version 1.0
 * @description: 说明描述
 * @date 2020/10/20 12:43
 */
@Service
public class PlayerService {

    @Autowired
    PlayerMapper playerMapper;


    public List<Player> getPlayerInfo(){
         return playerMapper.selectList(null);
    }

}
