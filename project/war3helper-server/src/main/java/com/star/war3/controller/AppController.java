package com.star.war3.controller;

import com.star.war3.entity.Player;
import com.star.war3.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * PlayerController
 *
 * @author ZJY
 * @version 1.0
 * @description: 说明描述
 * @date 2020/10/20 12:43
 */
@Controller
public class AppController {

    @Autowired
    PlayerService playerService;

    @RequestMapping("hello")
    @ResponseBody
    public String hello(){
        return "hello world！";
    }

    @GetMapping("/player/info")
    @ResponseBody
    public Object getPlayerInfo(){
        return playerService.getPlayerInfo();
    }


}
