package com.star.war3.config;

import com.star.war3.interceptor.CrossInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * SessionConfiguration
 *
 * @author ZJY
 * @version 1.0
 * @description: 说明描述
 * @date 2020/10/20 15:59
 */
@Configuration
public class SessionConfiguration extends WebMvcConfigurerAdapter {


    /**
     * 配置拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry){
        //registry.addInterceptor此方法添加拦截器
        registry.addInterceptor(new CrossInterceptor()).addPathPatterns("/**");
    }
}