package com.star.war3;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * com.star.com.war3.War3Server
 *
 * @author ZJY
 * @version 1.0
 * @description: 说明描述
 * @date 2020/10/19 16:58
 */

@SpringBootApplication
@Configuration
@MapperScan("com.star.war3.mapper")
public class War3Server {



    public static void main(String[] args) {
        SpringApplication.run(War3Server.class, args);
    }

}
