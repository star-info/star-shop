package com;

import com.star.war3.War3Server;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringBootTest(classes = War3Server.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class BaseApplicationTest {

    @Before
    public void init() {
        System.out.println("-------测试开始-------");
//        l1 = System.currentTimeMillis();
    }

    @After
    public void after() {
        System.out.println("-------测试结束-------");
//        l1 = System.currentTimeMillis() - l1;
//        System.out.println("总耗时： "+l1/1000+"."+l1%1000+"s");
    }

}
