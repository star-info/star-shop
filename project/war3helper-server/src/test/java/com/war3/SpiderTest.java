package com.war3;

import cn.hutool.http.HttpUtil;
import com.BaseApplicationTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * SpiderTest
 *
 * @author ZJY
 * @version 1.0
 * @description: 说明描述
 * @date 2020/10/21 14:24
 */
@Slf4j
public class SpiderTest extends BaseApplicationTest {

    @Test
    public void demo() {
        System.out.println(" 爬虫 demo ");
//        l1 = System.currentTimeMillis();


        String url = "www.baidu.com/s";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("wd", "boots");
        // 无参GET请求
        String result = HttpUtil.get("https://www.baidu.com");
        // 带参GET请求
        String result2 = HttpUtil.get(url, paramMap);

        System.out.println("无参GET==================");
        System.out.println(result);
        //System.out.println("带参GET==================");
        //System.out.println(result2);

    }

}
