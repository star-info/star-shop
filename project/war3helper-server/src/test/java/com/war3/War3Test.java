package com.war3;

import com.BaseApplicationTest;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.star.war3.entity.Player;
import com.star.war3.mapper.PlayerMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * com.war3.War3Test
 *
 * @author ZJY
 * @version 1.0
 * @description: 说明描述
 * @date 2020/10/20 11:35
 */
@Slf4j
public class War3Test extends BaseApplicationTest {

    @Test
    public void demo() {
        System.out.println(" 测试 demo ");
//        l1 = System.currentTimeMillis();
    }

    @Autowired
    PlayerMapper playerMapper;

    /**
     * player 增删改查
     */
    @Test
    public void playerCRUDTest(){


        /**
         *  查询 id=1 的Player
         */
        List<Player> list = playerMapper.selectList(Wrappers.<Player>lambdaQuery().eq(Player::getId,1));
        log.info("list {} ",list);


    }









}
