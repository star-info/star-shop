module.exports = {
    // 生产环境
    publicPath: process.env.NODE_ENV === 'production'
    ? '/admin/'
    : '/',

    devServer: {
        
        //配置跨域
        proxy: {
            '/api': {     //这里最好有一个 /
                //target:"http://192.168.1.107:9000",  // 真实地址
                //target:"http://localhost:8080",  // 真实地址
                target:"http://localhost:9001",  // 真实地址
                ws: true,        //如果要代理 websockets，配置这个参数
                secure: false,  // 如果是https接口，需要配置这个参数
                changeOrigin: true,  //是否跨域
                pathRewrite:{
                    '^/api':''
                }
            },
            
        }
    }
}