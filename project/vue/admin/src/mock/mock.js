import Mock from 'mockjs'
import common from '@/common/common.js'

var MOCK = true;
// mock get 请求demo
const dataRes = function (params) {

    console.log("============");
    console.log(JSON.parse(params.body));
    console.log("============");
    return {
        code: 0,
        msg: 'ok'
    }
}
// mock post 请求 demo
const dataRes2 = function (params) {
    console.log("============");
    console.log(JSON.parse(params.body));
    console.log("============");
    return {
        code: 0,
        msg: 'ok'
    }
}


const mockLogin = function (params) {
    // console.log("============");
    // console.log(JSON.parse(params.body));
    let form = JSON.parse(params.body);
    let result = {};
    if (form.username == 'admin' && form.password == '123') {

        // const jwt  = require('jsonwebtokens');
        // let payload = {name:'张三',admin:true};
        // let secret = 'I_LOVE_JING';
        // let token = jwt.sign(payload,secret);
        // console.log(token)
        let token = Mock.Random.string(32);

        result = { code: 0, msg: 'ok', token: token };
    }
    else {
        result = { code: 1, msg: '账号或密码不正确' };


    }

    return result;
}

let UserList = []
const count = 60

for (let i = 0; i < count; i++) {

    let randName = Mock.Random.name();
    let randCName = Mock.Random.cname();
    UserList.push(Mock.mock({
        // id: Mock.Random.guid(),
        id: i+1,
        name: randCName,
        username: randName,
        addr: Mock.mock('@county(true)'),
        date: Mock.Random.date(),
        email:Mock.Random.email(),
        mobile: (100 + Mock.Random.integer(0, 99)) * 10000 * 10000 + Mock.Random.integer(0, 10000 * 10000),
        'age|18-60': 1,
        birth: Mock.Random.date(),
        sex: Mock.Random.integer(0, 1),
		isLock:Mock.Random.boolean()
    }))
}

// import textJsom from "@/static/json/users.json"
const mockUserList = function (params) {
    
    
    //console.log(params);
    let realUrl = decodeURIComponent(params.url);
    let page = common.getUrlParam(realUrl, 'page');
    let limit = common.getUrlParam(realUrl, 'pageSize');
    let query = common.getUrlParam(realUrl, 'query');

    let pageList = UserList;
    //console.log("query for " + query+" page "+page);
    pageList = pageList.filter(user => {
        if (query && user.username.indexOf(query) === -1) {
            return false
        }

        return true
    })

    // 分页过滤
    pageList = pageList.filter((item, index) => index < limit * page && index >= limit * (page - 1))
    return {
        list: pageList,
        count: UserList.length
    };

}

const deleteUser = function (params) {    
    let id = common.getUrlId(params.url);
	console.log("Mock服务器 删除 user "+id);
    return {
        result:'ok'
    }
}

const newOrEditeUser = function (params) {
    // console.log("============");
    // console.log(JSON.parse(params.body));
    let form = JSON.parse(params.body);
    console.log("Mock服务器 新增/修改 user ");
    console.log(form);
    return {
        result:'ok'
    }
}


if (MOCK) {
   

    Mock.mock("http://localhost:8080/api/mock", 'get', dataRes);
    Mock.mock("http://localhost:8080/api/mockp", 'post', dataRes2);

    Mock.mock("http://localhost:8080/api/login", 'post', mockLogin);

    Mock.mock("http://localhost:8080/api/users/ping", 'get', { data: "aaa" });

    Mock.mock(RegExp("http://localhost:8080/api/users" + ".*"), 'get', mockUserList);
    Mock.mock(RegExp("http://localhost:8080/api/users" + ".*"), 'delete', deleteUser);
    Mock.mock(RegExp("http://localhost:8080/api/users" + ".*"), 'post', newOrEditeUser);

    

}

