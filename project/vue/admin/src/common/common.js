


export default {
    getToken: function () {
        const token = localStorage.getItem('token');
        return token;
    },
    getUrlParam: function(url, paraName) {
        var arrObj = url.split("?");

        if (arrObj.length > 1) {
            var arrPara = arrObj[1].split("&");
            var arr;

            for (var i = 0; i < arrPara.length; i++) {
                arr = arrPara[i].split("=");

                if (arr != null && arr[0] == paraName) {
                    return arr[1];
                }
            }
            return null;
        }
        else {
            return null;
        }
    },
	/**
	 *  返回rest风格的url最后的id
	**/
    getUrlId:function(url){
        let reg = /\/[1-9][0-9]*/.exec(url);
		let ret = reg[reg.length-1];
		return ret.substring(1,ret.length);
    },
    
}