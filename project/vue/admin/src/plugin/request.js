/**
 *  axios 封装
 */
import axios from 'axios'
import common from '@/common/common.js'

const HttpRequest = {}

// axios 拦截器
axios.interceptors.request.use(
  config => {
	// 获取token并放入请求头
	let token = common.getToken();
    if (token) {
		config.headers['accessToken'] = token;
		config.headers['Authorization'] = token;
	}
    if (config.method == 'post') {
      config.data = {
        ...config.data,
        t: Date.parse(new Date()) / 1000
      }
    } else if (config.method == 'get') {
      config.params = {
        t: Date.parse(new Date()) / 1000,
        ...config.params
      }
    }
    return config
  },
  error => {
    console.log(error)
    return Promise.reject(error)
  }
)


HttpRequest.install = (Vue)=>{
    axios.defaults.baseURL='http://localhost:8080/api';
    //axios.defaults.baseURL='/api';
    Vue.prototype.$http=axios;
}

export default HttpRequest;