
import Vue from 'vue'
import VueRouter from 'vue-router'
// import HelloWorld from '../components/HelloWorld.vue'
import Home from '@/views/Home.vue'
import About from '@/views/About.vue'
import CartView from '@/views/CartView.vue'
import Login from '@/views/Login.vue'
import Test from '@/views/Test.vue'
import User from '@/components/user/users.vue'
import UserStats from '@/components/user/user_stats.vue'
import DefaultPage from '@/views/Default.vue'
Vue.use(VueRouter)
const routers = 
    [
        { 
            path: '/', 
            redirect:{name:'home'}
        },
        { 
            path: '/about', 
            name: 'about',
            component: About 
        },
        { 
            path: '/test', 
            name: 'test',
            component: Test 
        },
        { 
            path: '/home', 
            name: 'home',
            component: Home,
			meta:{
				title:'管理台首页',
				auth:true
			},
            children:[
                {
                    path:'/default',
                    name: 'default',
                    component: DefaultPage,
					meta:{
						title:'概览'
					},
                },
                {
                    path: '/users', 
                    name: 'users',
                    component: User,
					meta:{
						title:'用户管理'
					},
                },
                {
                    path: '/user_stats', 
                    name: 'user_stats',
                    component: UserStats,
                }
            ]
        },
        {
            path: '/login', 
            name: 'login',
            component: Login 
        },
        { 
            path: '/cart', 
            name:'cart',
            component: CartView 
        },
        {
            path: '/*', 
            component: ()=>import('@/views/Error.vue')
        }
    ]
const router = new VueRouter({
	base: '/admin',
    mode:'history', // 默认H5 模式 
    routes:routers
});
export default router