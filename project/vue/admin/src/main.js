import Vue from 'vue'
import App from './App.vue'

import './plugin/element.js'

import router from './router'
import Myhttp from './plugin/request.js'

Vue.use(Myhttp);
import '@/assets/css/reset.css'

import './mock/mock'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: { // 装谈
    count: 0
  },
  mutations: { // 同步方法
    increment (state) {
      state.count++
	}
  }
  // actionss 异步方法
  
})

router.beforeEach((to,from,next)=>{
	if(to.meta.title){
		document.title = to.meta.title; 
	}
	// 鉴权处理
	// if(to.meta.auth){
	// 	console.log("xxx");
	// }
	next();
})

Vue.config.productionTip = false
new Vue({
  router,
  store: store,
  render: h => h(App),
}).$mount('#app')
