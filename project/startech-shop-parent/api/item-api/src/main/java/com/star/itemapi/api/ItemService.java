package com.star.itemapi.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * 
 * 功能说明:商品服务
 * 
 * @author zjy
 *
 */
public interface ItemService {

	/**
	 * 测试
	 *
	 * @return
	 */
	@RequestMapping("/ping")
	Map ping();

	/**
	 * 首页展示商品
	 * 
	 * @return
	 */
	@RequestMapping("/indexItems")
	Map<String, Object> getIndexItem();

	/**
	 * 查询商品
	 * 
	 * @return
	 */
	@RequestMapping("/items")
	Map<String, Object> geItem(@RequestParam("id") Long id);

	/**
	 * 查询商品详情
	 *
	 * @return
	 */
	@RequestMapping("/itemDetail")
	Map<String, Object> getItemDesc(@RequestParam("id") Long id);



}
