package com.star.memberapi.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description: Demo
 * @author: zjy
 * @create: 2020-07-08 18:41
 */
@RequestMapping("/demo")
public interface DemoApi {

    @GetMapping("/")
    String index();

    @GetMapping("/info")
    Object info();

    @GetMapping("/setRedisKey")
    Map setRedisKey(String key, String value);

    @GetMapping("/getRedisKey")
    Map getRedisKey(String key);

}
