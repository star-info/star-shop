package com.star.memberapi.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 23:48
 */
@Data
public class UserAuthDTO {

    private Long id;
    @NotNull(message = "用户名不能为空")
    private String  username;
    private String  password;
    @NotNull(message = "邮箱不能为空")
    private String  email;
    private String  mobile;
    private Timestamp createdTime;


}
