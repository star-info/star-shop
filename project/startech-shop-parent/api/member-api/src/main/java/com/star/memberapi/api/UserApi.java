package com.star.memberapi.api;

import com.star.memberapi.dto.UserAuthDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description: 会员服务Api
 * @author: zjy
 * @create: 2020-07-13 13:31
 */
public interface UserApi {

    /**
     * 测试
     * @return
     */
    @GetMapping("/ping")
    @ResponseBody
    Map ping();

    /**
     * 通过Id查询
     * @param userId
     * @return
     */
    @GetMapping("/users")
    @ResponseBody
    UserAuthDTO getUserInfoById(@RequestParam("id")Integer userId);

    /**
     * 注册
     * @param userAuthDto
     * @return
     */
    @PostMapping("/user")
    Map insertUser(UserAuthDTO userAuthDto);

    /**
     * 用户登陆
     * @param userAuthDto
     * @return
     */
    @PostMapping("/login")
    Map login(UserAuthDTO userAuthDto);

}
