package com.star.adapter;

import com.alibaba.fastjson.JSONObject;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-12 02:07
 */
public interface MessageAdapter {
    public void distribute(JSONObject jsonObject);
}
