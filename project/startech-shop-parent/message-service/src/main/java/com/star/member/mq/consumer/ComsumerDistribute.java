package com.star.member.mq.consumer;

import com.alibaba.fastjson.JSONObject;
import com.star.memberapi.dto.UserAuthDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @program: startech-shop-parent
 * @description: mq 从队列中获取
 * @author: zjy
 * @create: 2020-07-12 11:26
 */
@Slf4j
@Component
public class ComsumerDistribute {

    @JmsListener(destination = "mail_queue")
    public void distribute(String json){
        log.info("消息服务 收到消息，内容json :{}",json);

        UserAuthDTO userAuthDto = JSONObject.parseObject(json, UserAuthDTO.class);
        log.info(" {}",userAuthDto);
        String email = userAuthDto.getEmail();
        if(!email.isEmpty()){
            // 发送邮件
            log.info("email is {}",email);
        }
        
    }
}
