package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-08 13:35
 */
@SpringBootApplication
// 是否要注册的eureka 如果不开启注解则当作普通SpringBoot项目启动
//@EnableEurekaClient
//@MapperScan("com.star.member.mapper")
public class MessageServer {
    public static void main(String[] args){
        SpringApplication.run(MessageServer.class,args);
    }

}
