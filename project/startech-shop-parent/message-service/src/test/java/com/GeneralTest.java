package com;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-11 21:44
 */
@Slf4j
public class GeneralTest extends BaseApplicationTest {

    /**
     * 邮件发送(代码需要整理)
     */
    @Test
    public void t2(){
        try {
            //sendMail("394341810@qq.com","***");
            sendMail("ssszxd_88@163.com","白痴你好！","你是大白痴！！！！");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendMail(String to,String subject,String code) throws Exception {
        //获得session对象
        final Properties props = new Properties();

        //下面两段代码是设置ssl和端口，不设置发送不出去。
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        // 发送服务器需要身份验证
        props.setProperty("mail.transport.protocol", "smtp");// 发送邮件协议名称
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.qq.com");//QQ邮箱的服务器 如果是企业邮箱或者其他邮箱得更换该服务器地址
        // 发件人的账号
        props.put("mail.user", "394341810@qq.com");
        // 访问SMTP服务时需要提供的密码
        props.put("mail.password", "mlyutarjuurdbici");
        // 构建授权信息，用于进行SMTP进行身份验证
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                // 用户名、密码
                String userName = props.getProperty("mail.user");
                String password = props.getProperty("mail.password");
                //发件邮箱
                return new PasswordAuthentication(userName, password);
            }
        };
        Session se = Session.getInstance(props,authenticator);
        //创建一个表示邮件的对象message
        Message mes = new MimeMessage(se);
        try {
            mes.setFrom(new InternetAddress("394341810@qq.com"));//发件人
            mes.addRecipient(Message.RecipientType.TO, new InternetAddress(to));//设置收件人
            mes.setSubject(subject);//主题
            mes.setContent(code,"text/html;charset=UTF-8");
            //发送邮件transport
            Transport.send(mes);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
