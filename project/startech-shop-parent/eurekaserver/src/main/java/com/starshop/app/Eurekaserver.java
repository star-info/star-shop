package com.starshop.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-08 13:35
 */
@SpringBootApplication
@EnableEurekaServer
public class Eurekaserver {
    public static void main(String[] args){
        SpringApplication.run(Eurekaserver.class,args);
    }

}
