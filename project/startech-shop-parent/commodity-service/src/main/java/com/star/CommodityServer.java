package com.star;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author a
 */
@SpringBootApplication
@EnableEurekaClient
public class CommodityServer {

	public static void main(String[] args) {
		SpringApplication.run(CommodityServer.class, args);
	}

}
