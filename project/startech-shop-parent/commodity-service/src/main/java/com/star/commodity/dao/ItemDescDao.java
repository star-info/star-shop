package com.star.commodity.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.commodity.model.po.ItemDetail;
import org.apache.ibatis.annotations.Mapper;


/**
 * 商品详情
 * @author zjy
 */
@Mapper
public interface ItemDescDao extends BaseMapper<ItemDetail> {

//	/**
//	 *  获取商品详情
//	 *
//	 * @param id
//	 * @return
//	 */
//	@Select(" SELECT id AS id , description as itemDesc , created as created,updated as updated FROM item_detail  where id=#{id} ")
//	ItemDetail getItemDesc(@Param("id") Long id);


}
