package com.star.commodity.model.po;

import com.star.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 商品类型
 * 
 * @author Administrator
 *
 */
@Getter
@Setter
public class Category
		extends BaseEntity
{


	private Long parentId;

	private String name;

	private String img;

	private Integer status;

	private Integer sortOrder;

	private Boolean isParent;

	private Date created;

	private Date updated;
}
