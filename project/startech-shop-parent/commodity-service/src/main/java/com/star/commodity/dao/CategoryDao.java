package com.star.commodity.dao;

import com.star.commodity.model.po.Category;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import java.util.List;

/**
 * @author a
 */
@Mapper
public interface CategoryDao {
	@Select("select id as id , parent_id as parentId ,name as name, img as img,status as status , is_parent as  isParent, created as created ,updated as updated from category")
	 List<Category> allItemCat();
}
