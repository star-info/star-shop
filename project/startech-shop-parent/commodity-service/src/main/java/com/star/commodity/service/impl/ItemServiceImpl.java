package com.star.commodity.service.impl;


import com.star.commodity.dao.CategoryDao;
import com.star.commodity.dao.ItemDao;
import com.star.commodity.dao.ItemDescDao;
import com.star.commodity.model.po.Category;
import com.star.commodity.model.po.ItemDetail;
import com.star.commodity.model.po.ItemEntity;
import com.star.common.service.BaseApiService;
import com.star.common.constants.Constants;
import com.star.itemapi.api.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author a
 */
@RestController
public class ItemServiceImpl extends BaseApiService implements ItemService {
	@Autowired
	private ItemDao itemDao;
	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private ItemDescDao itemDescDao;


	@Override
	public Map ping() {
		Map m = new HashMap(Constants.MAP_INITIAL_CAPACITY);
		m.put("name","item-service");
		m.put("version","1.0.0");
		m.put("status","ok");
		return  m;
	}

	@Override
	public Map<String, Object> getIndexItem() {
		// 查询所有的类型
		List<Category> listItemCat = categoryDao.allItemCat();
		Map<String, Object> result = new HashMap<>(16);
		for (Category category : listItemCat) {
			Long id = category.getId();
			String name = category.getName();
			List<ItemEntity> listItem = itemDao.getIndexItem(id);
			if (!(listItem.isEmpty() && listItem.size() <= 0)) {
				result.put(name, listItem);
			}

		}
		return setResultOk(result);
	}

	@Override
	public Map<String, Object> geItem(@RequestParam("id") Long id) {
		ItemEntity item = itemDao.getItem(id);
		if(item==null){
			return setResultError("没有查询到结果");
		}
		return setResultOk(item);
	}

	@Override
	public Map<String, Object> getItemDesc(Long id) {
		ItemDetail itemDesc = itemDescDao.selectById(id);
		return setResultOk(itemDesc);
	}

}
