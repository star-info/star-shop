package com.star.commodity.model.po;

import com.star.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 商品详情
 * @author a
 */
@Getter
@Setter
public class ItemDetail
		extends BaseEntity
{

	private String description;

}
