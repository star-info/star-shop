package com.star.commodity.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.commodity.model.po.ItemEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author a
 */
@Mapper
public interface ItemDao extends BaseMapper {

	/**
	 * 首页商品
	 * @param id
	 * @return
	 */
	@Select("select a.id as id ,a.title as title ,a.sell_point as sellPoint, a.price as price,a.num as num,a.barcode as barcode,a.image as image ,a.parent_id as parentId,a.cid as cid,a.status as status,a.created as created,a.updated as updated from item as a inner join  category as b on a.parent_id=b.id where b.id =#{id} LIMIT 0,8")
	List<ItemEntity> getIndexItem(@Param("id") Long id);

	/**
	* 通过ID查询商品
	* @param: [id]
	* @return: com.star.commodity.model.po.ItemEntity
	* @Author: zjy
	* @Date: 2020/7/20
	*/
	@Select("select a.id as id ,a.title as title ,a.sell_point as sellPoint, a.price as price,a.num as num,a.barcode as barcode,a.image as image ,a.parent_id as parentId,a.cid as cid,a.status as status,a.created as created,a.updated as updated from item as a where a.id =#{id}")
	ItemEntity getItem(@Param("id") Long id);
}
