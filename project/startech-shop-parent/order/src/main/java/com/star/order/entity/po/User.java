package com.star.order.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.star.common.entity.BasePO;
import lombok.Data;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-08-17 14:55
 */
@Data
@TableName("`userauth`")
public class User extends BasePO {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    private String username;
}
