package com.star.order.mapper;

//import com.baomidou.dynamic.datasource.annotation.DS;
//import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.order.entity.po.Order;
import com.star.order.entity.po.User;


/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 09:17
 */
//@DS("sharding")
public interface UserMapper extends BaseMapper<User> {


}
