package com.star.order.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.star.common.entity.BasePO;
import lombok.Data;

import java.util.Date;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 09:29
 */
@Data
@TableName("`order`")
public class Order extends BasePO {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    private Long  orderId;
    private Double  amount;
    private Date createdDate;
}
