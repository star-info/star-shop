package com;

import cn.hutool.core.util.RandomUtil;
import com.star.order.entity.po.Order;
import com.star.order.entity.po.User;
import com.star.order.mapper.OrderMapper;
import com.star.order.mapper.UserMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-08-17 13:33
 */
public class MultiDBTest extends BaseApplicationTest{

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    UserMapper userMapper;

    @Test
    public void t1() {

        // 插入
        for(int i = 0;i < 10;i++){
            Order o = new Order();
            //o.setOrderId((long)i);
            o.setAmount(RandomUtil.randomDouble(0,100));
            o.setCreatedDate(new Date());
            orderMapper.insert(o);
        }
    }

    @Test
    public void t2() {

        // 查询
        List<Order> info = orderMapper.selectList(null);
        System.out.println(info);

    }

    @Test
    public void t3() {
        List<User> info3 = userMapper.selectList(null);
        System.out.println(info3);
    }
}
