package com.star;

import com.star.gateway.filter.ErrorFilter;
import com.star.gateway.filter.PreFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-08 13:35
 */
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@SpringBootApplication
// 是否要注册的eureka 如果不开启注解则当作普通SpringBoot项目启动
@EnableEurekaClient
@EnableZuulProxy
public class ApiGatewayServer {

    @Autowired
    PreFilter preFilter;

    @Autowired
    ErrorFilter errorFilter;

    public static void main(String[] args){
        SpringApplication.run(ApiGatewayServer.class,args);
    }

}
