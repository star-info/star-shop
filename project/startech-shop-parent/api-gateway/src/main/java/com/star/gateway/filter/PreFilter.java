package com.star.gateway.filter;

import com.alibaba.fastjson.JSON;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.star.common.constants.Constants;
import com.star.common.util.CookieUtil;
import com.star.gateway.constants.GatewayConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description:
 *      PRE： 这种过滤器在请求被路由之前调用。我们可利用这种过滤器实现身份验证、在集群中选择请求的微服务、记录调试信息等。
 * @author: zjy
 * @create: 2020-07-24 14:14
 */
@Component
@Slf4j
public class PreFilter extends ZuulFilter {

    /**
     * 过滤器的类型 决定过滤的请求在哪个生命周期执行
     * pre表示请求在路由之前被执行
     * @return
     */
    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    /**
     * 过滤器的执行顺序 优先级越小越先被执行
     * @return
     */
    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * 判断过滤器是否需要执行 直接返回true表示所有请求都生效
     * @return
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    private String getRootPath(String uri){
        if(uri!=null){
            String[] uriStr = uri.split("/");
            if(uriStr.length>2) {
                return uriStr[1];
            }
        }
        return null;
    }
    /**
     * 判断服务权限 后期应该从数据库读
     * @param serviceName
     * @param uri
     * @return
     */
    private boolean hasAuthority(String uri,String serviceName){

        String view      = "mobile-view";
        String apiItem   = "api-item";
        String apiMember = "api-member";

        // 浏览器层（Postman） 只能访问 web层
        if(serviceName.isEmpty()){
            // 浏览器 外网
            String rootPath = getRootPath(uri);
            if(rootPath.equals(view)){
                return true;
            }
            return  false;
        }

        // web 层 能访问 member、commodity
        if(serviceName.equals(GatewayConstants.SHOP_WEB) || serviceName.equals(GatewayConstants.POST_MAN)){
            String rootPath = getRootPath(uri);
            if(rootPath.equals(apiItem)||rootPath.equals(apiMember)){
                return true;
            }
            return false;
        }



        return false;
    }

    /**
     * 过滤器具体逻辑
     * @return
     */
    @Override
    public Object run() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        log.info(" >>> Api Gateway Pre==============================");
        log.info("请求方法 {} ",request.getMethod());
        log.info("请求url {} ",request.getRequestURL());
        log.info("请求uri {} ",request.getRequestURI());



        String refer=request.getHeader("from");
        String serviceName =request.getHeader("name");

        if(refer==null || serviceName==null){
            refer = "";
            serviceName = "";
        }

        // uri 鉴权
        if(!hasAuthority(request.getRequestURI(),serviceName)){
            log.info("越权访问！");
            // 过滤该请求，不对其进行路由
            ctx.setSendZuulResponse(false);
            //返回错误代码
            ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
            try {
                Map m = new HashMap(Constants.MAP_INITIAL_CAPACITY);
                m.put("from","Gateway");
                m.put("msg","越权访问！");
                ctx.getResponse().setCharacterEncoding("utf-8");
                ctx.getResponse().setContentType("application/json; charset=utf-8");

                ctx.getResponse().getWriter().write(JSON.toJSONString(m));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        log.info("请求来源 {} {} ",refer,serviceName);


        if(tokenCheck(serviceName)){

            Map m = new HashMap(Constants.MAP_INITIAL_CAPACITY);
            m.put("from","Gateway");
            m.put("msg","用户未授权！");
            ctx.getResponse().setCharacterEncoding("utf-8");
            ctx.getResponse().setContentType("application/json; charset=utf-8");

            try {
                ctx.getResponse().getWriter().write(JSON.toJSONString(m));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        request.setAttribute("gateway-info","ok");
        log.info(" >>> ==========================================");
        return null;
    }

    /***
     * token 校验
     * @return
     */
    private Boolean tokenCheck(String servicename){

        // 是否是允许不登陆的Path

        // token校验
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String token = CookieUtil.getUid(request, Constants.COOKIE_TOKEN);
        if(token == null || token.isEmpty()){
            log.info("token is null or empty!");
            return false;
        }

        log.info("token is {} ",token);
        return true;
    }

}
