package com.star.gateway.constants;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-24 23:34
 */
public class GatewayConstants {
    public static final String SHOP_WEB = "shop-web";
    public static final String POST_MAN = "postman";
}
