package com.star.member.manager.shiro;

import cn.hutool.crypto.SecureUtil;
import com.star.member.entity.po.UserAuth;
import com.star.member.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-27 02:16
 */
@Component
@Slf4j
public class UserRealm extends AuthorizingRealm {

    @Autowired
    UserService userService;

    /**
     * 执行授权逻辑
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        log.info("执行授权逻辑");

        // 获取主身份信息
        UserAuth userAuth = (UserAuth)principalCollection.getPrimaryPrincipal();
        log.info("username {} ",userAuth);

        // 从数据库中获取权限信息

        // 向用户添加权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addStringPermission("auth:get");

        simpleAuthorizationInfo.addStringPermission("data:read");

        return simpleAuthorizationInfo;
    }

    /**
     * 执行认证逻辑
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        log.info("执行认证逻辑");
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken)authenticationToken;
        UserAuth userAuth = userService.selectUserByName(usernamePasswordToken.getUsername());
        if(userAuth == null){
            return null;
        }
        // 使用用户名作为盐值
        ByteSource byteSource = ByteSource.Util.bytes(userAuth.getUsername());
        return new SimpleAuthenticationInfo(userAuth, userAuth.getPassword(), byteSource, "");
    }

    @Override
    protected Object getAuthorizationCacheKey(PrincipalCollection principals) {

        UserAuth userAuth = (UserAuth)principals.getPrimaryPrincipal();
        if(userAuth!=null){
            userAuth.getUsername();
        }
        return userAuth;
    }
}
