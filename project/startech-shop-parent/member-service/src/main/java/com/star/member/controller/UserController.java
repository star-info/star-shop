package com.star.member.controller;

import com.star.common.constants.Constants;
import com.star.common.util.ConvertUtil;
import com.star.member.entity.po.UserAuth;
import com.star.memberapi.api.UserApi;
import com.star.memberapi.dto.UserAuthDTO;
import com.star.member.model.bo.LoginResultBo;
import com.star.member.service.UserService;
import com.star.common.service.BaseApiService;
import com.star.common.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-08 19:06
 */
@RestController("/api-member")
@Slf4j
public class UserController extends BaseApiService implements UserApi {

    @Autowired
    private UserService userService;

    @Override
    public Map ping(){
        Map m = new HashMap(Constants.MAP_INITIAL_CAPACITY);
        m.put("name","member-service");
        m.put("version","1.0.1");
        m.put("status","ok");
        return m;
    }

    @Override
    public UserAuthDTO getUserInfoById(Integer id) {
        UserAuth userAuth = userService.selectUserById(id);
        UserAuthDTO userAuthDTO = ConvertUtil.dtoConvert(userAuth,UserAuthDTO.class);
        return userAuthDTO;
    }

    @Override
    public Map insertUser(@RequestBody  @Valid UserAuthDTO userAuthDto) {
        try{
            userService.addUser(userAuthDto);
            return setResultOk();
        }catch (Exception e){
            e.printStackTrace();
            throw new BusinessException("业务异常 "+e.getMessage());
        }
    }

    @Override
    public Map login(@RequestBody UserAuthDTO userAuthDto) {

        // 从数据库查找数据 密码比对
        LoginResultBo bo = userService.login(userAuthDto);
        if(!bo.isLoginSuccess()){
            return setResultError(bo.getMsg());
        }

        Map info = new HashMap();
        info.put("token",bo.getToken());
        // key为自定义令牌
        return setResultOk(info);
    }

}
