package com.star.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.member.entity.po.UserAuth;


/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 09:17
 */
public interface UserMapper extends BaseMapper<UserAuth> {
}
