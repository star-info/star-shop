package com.star.tools;

import com.star.member.constants.ShiroConstants;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

import java.util.Scanner;

/**
 * @program: startech-shop-parent
 * @description: 密码密文生成工具
 * @author: zjy
 * @create: 2020-07-28 17:34
 */
public class PasswordEncryptTools {


    public static void main(String[] args) {
        System.out.println("请输入用户名：");
        Scanner input1=new Scanner(System.in);
        String username = input1.next();
        System.out.println("请输入要加密的密码：");
        Scanner input2=new Scanner(System.in);
        String password = input2.next();

        // 使用用户名作为盐值
        ByteSource byteSource = ByteSource.Util.bytes(username);
        Object obj = new SimpleHash(ShiroConstants.HASH_ALGORITHM_NAME, password, byteSource, ShiroConstants.HASH_ITERATIONS);
        System.out.println("密码密文是 :"+obj);

    }
}
