package com.star.member.service;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.star.common.constants.Constants;
import com.star.common.redis.BaseRedisService;
import com.star.member.model.bo.LoginResultBo;
import com.star.memberapi.dto.*;
import com.star.member.entity.po.UserAuth;
import com.star.member.mapper.UserMapper;
import com.star.member.mq.producer.RegisterMailBoxProducer;
import com.star.common.util.ConvertUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.jms.Destination;
import java.util.List;
import java.util.UUID;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 15:05
 */
@Service
@Slf4j
public class UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    RegisterMailBoxProducer registerMailBoxProducer;

    @Autowired
    private BaseRedisService baseRedisService;

    /**
     * 消息队列名称
     */
    @Value("${message.queue}")
    String MESSAGE_QUEUE;

    public List<UserAuth> getUserList(){
        return userMapper.selectList(null);
    }

    public void addUser(UserAuthDTO userAuthDto){
        // 通过转换工具转换
        UserAuth userAuth = ConvertUtil.dtoConvert(userAuthDto,UserAuth.class);
        userMapper.insert(userAuth);

        // 发送消息
        Destination destination = new ActiveMQQueue(MESSAGE_QUEUE);
        registerMailBoxProducer.send(destination,userAuthDto);
    }

    /**
     * 后期改成shiro
     * @param userAuthDTO
     * @return
     */
    @Deprecated
    public LoginResultBo login(UserAuthDTO userAuthDTO){
        UserAuth user = selectUserByName(userAuthDTO.getUsername());
        LoginResultBo loginResultBo = new LoginResultBo();
        loginResultBo.setLoginSuccess(true);

        // 用户不存在
        if(user == null){
            log.info("用户 {} 不存在",userAuthDTO.getUsername());
            loginResultBo.setMsg("用户 "+userAuthDTO.getUsername()+" 不存在!");
            loginResultBo.setLoginSuccess(false);
            return loginResultBo;
        }

        // 将密码进行MD5加密
        String md5pwd = SecureUtil.md5(userAuthDTO.getPassword());

        // 密码不对
        if(!user.getPassword().equals(md5pwd)){
            log.info("密码不正确！");
            loginResultBo.setMsg("密码不正确！");
            loginResultBo.setLoginSuccess(false);
            return loginResultBo;
        }

        // 生成对应的token
        String token = UUID.randomUUID().toString();
        // redis存token
        baseRedisService.set(token,user.getId().toString(), Constants.USER_TOKEN_TEAMVALIDITY);
        loginResultBo.setMsg("ok");
        loginResultBo.setToken(token);
        return loginResultBo;
    }

    /**
     * 通过用户名搜索用户
     * @param userName
     * @return
     */
    public UserAuth selectUserByName(String userName){
        return userMapper.selectOne(Wrappers.<UserAuth>lambdaQuery().eq(UserAuth::getUsername,userName));
    }

    public UserAuth selectUserById(Integer id){
        return userMapper.selectOne(Wrappers.<UserAuth>lambdaQuery().eq(UserAuth::getId,id));
    }
}
