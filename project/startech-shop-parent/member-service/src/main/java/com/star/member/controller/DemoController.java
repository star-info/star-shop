package com.star.member.controller;

import com.star.memberapi.api.DemoApi;
import com.star.member.service.UserService;
import com.star.common.service.BaseApiService;
import com.star.common.redis.BaseRedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: member-service
 * @description:
 * @author: zjy
 * @create: 2020-07-08 19:06
 */
@RestController
@Slf4j
public class DemoController extends BaseApiService implements DemoApi {

    @Autowired
    private BaseRedisService baseRedisService;

    @Autowired
    private UserService userService;

    @Override
    public String index() {
        return "This is demo";
    }

    @Override
    public Object info() {
        log.info("xxxxxxxxxxxxxxxxxx ");
        return setResultOk();
    }

    @Override
    public Map setRedisKey(String key, String value) {
        baseRedisService.setString(key,value);
        return setResultOk();
    }

    @Override
    public Map getRedisKey(String key) {
        Object val = baseRedisService.get(key);
        Map m = new HashMap();
        m.put(key,val);
        return  setResultOk(m);
    }
    @GetMapping("/auth/a")
    public String authA(){
        return "authA ok";
    }
    @GetMapping("/auth/b")
    public String authB(){
        return "authB ok";
    }

    @GetMapping("/no_auth")
    public String noauth(){
        return "no Auth";
    }
}
