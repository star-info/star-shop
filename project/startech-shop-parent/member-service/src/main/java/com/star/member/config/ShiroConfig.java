package com.star.member.config;

import com.star.member.constants.ShiroConstants;
import com.star.member.manager.shiro.UserRealm;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-27 15:21
 */
@Configuration
@Slf4j
public class ShiroConfig {

    // session 保存时间 24小时
    final static int SESSION_EXPIRE = 3600 * 24;

    @Bean
    protected ShiroFilterFactoryBean ShiroFilterFactoryBean(@Qualifier("securityManager") DefaultWebSecurityManager dwsm) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // 设置安全管理器
        shiroFilterFactoryBean.setSecurityManager(dwsm);

        // 添加shiro内置过滤器
        // anon 无需认证
        // authc 必须认证
        // user 使用remmemberMe
        // perms 资源授权
        // role  角色授权

        Map<String,String> filterMap = new LinkedHashMap<>();
        // 过滤的是请求路由地址
        filterMap.put("/shiro/auth/2","perms[auth2:get]");

        // 对post 请求做拦截
        filterMap.put("/shiro/auth/data","rest[data:create]");


        filterMap.put("/shiro/auth/**","authc");
        filterMap.put("/shiro/auth/annon","anon");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterMap);
        // 未授权将跳转的登陆Url
        shiroFilterFactoryBean.setLoginUrl("/shiro/login");

        shiroFilterFactoryBean.setUnauthorizedUrl("/shiro/no_auth");

        return shiroFilterFactoryBean;
    }

    /**
     * 创建DefaultWebSecurityManager
     */
    @Bean(name="securityManager")
    @DependsOn("redisManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm")UserRealm userRealm){

        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        // 如果多个Realm 使用 .setRealms
        defaultWebSecurityManager.setRealm(userRealm);

        //CookieRememberMeManager rememberMeManager = (CookieRememberMeManager)defaultWebSecurityManager.getRememberMeManager();
        // 设置记住我的最大时间
        //rememberMeManager.getCookie().setMaxAge();

        // 缓存管理
        defaultWebSecurityManager.setCacheManager(redisCacheManager());

        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        sessionManager.setSessionDAO(redisSessionDAO());

        // 会话管理
        defaultWebSecurityManager.setSessionManager(sessionManager);

        return defaultWebSecurityManager;
    }


    /**
     * 创建Realm
     */
    @Bean(name="userRealm")
    public UserRealm getRealm(){
        UserRealm userRealm = new UserRealm();

        // Hash算法设置为MD5
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher(ShiroConstants.HASH_ALGORITHM_NAME);
        // 迭代次数
        hashedCredentialsMatcher.setHashIterations(ShiroConstants.HASH_ITERATIONS);
        userRealm.setCredentialsMatcher(hashedCredentialsMatcher);

        return userRealm;
    }

    RedisManager redisManager;
    @Bean("redisManager")
    public RedisManager redisManager(
            @Value("${spring.redis.host}")String redisHost,
            @Value("${spring.redis.port}")String redisPort,
            @Value("${spring.redis.password}")String password,
            @Value("${spring.redis.database}")String database
    ) {
        log.info("init redis manager" + redisHost + ":" + redisPort);
        RedisManager redisManager = new RedisManager();
        redisManager.setHost(redisHost + ":" + redisPort);
        redisManager.setPassword(password);
        redisManager.setDatabase(Integer.valueOf(database));
        this.redisManager = redisManager;
        return redisManager;
    }


    public RedisCacheManager redisCacheManager(){
        RedisCacheManager redisCacheManager = new RedisCacheManager();
        redisCacheManager.setRedisManager(redisManager);
        return redisCacheManager;
    }

    public RedisSessionDAO redisSessionDAO(){
        RedisSessionDAO redisSessionDAO = new RedisSessionDAO();
        redisSessionDAO.setRedisManager(redisManager);
        redisSessionDAO.setExpire(SESSION_EXPIRE);
        return redisSessionDAO;
    }

}
