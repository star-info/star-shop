package com.star.member.mq.producer;

import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Destination;


/**
 * @program: startech-shop-parent
 * @description: 消息服务 推送邮件信息
 * @author: zjy
 * @create: 2020-07-11 20:10
 */
@Service("registerMailBoxProducer")
public class RegisterMailBoxProducer {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    public void send(Destination destination, Object o){

        jmsMessagingTemplate.convertAndSend(destination,JSONObject.toJSONString(o));
    }



}
