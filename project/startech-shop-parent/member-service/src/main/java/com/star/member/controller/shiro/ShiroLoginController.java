package com.star.member.controller.shiro;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @program: startech-shop-parent
 * @description: shiro 案例
 * @author: zjy
 * @create: 2020-07-27 22:39
 */
@Slf4j
@Controller
@RequestMapping("/shiro")
public class ShiroLoginController {

    @PostMapping("/login")
    public String login(String username, String password, RedirectAttributes attributes){

        /**
         * Subject
         */
        Subject subject = SecurityUtils.getSubject();

        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        // 记住我
        // token.setRememberMe();
        try{

            subject.login(token);

            // jsession id 可以改为access token
            String sessionId = (String) subject.getSession().getId();
            log.info("jsessionId is {} ",sessionId);

            // Session session = subject.getSession();

            log.info("ok");
            //attributes.addFlashAttribute("id",1);
            return "redirect:/shiro/auth/1";
        }catch (UnknownAccountException e){
            // 用户名不存在
            log.info("用户名不存在");
            attributes.addFlashAttribute("msg","用户名不存在");
            return "redirect:/shiro/login";
        }catch(IncorrectCredentialsException ie){
            log.info("密码不正确");
            attributes.addFlashAttribute("msg","密码不正确");
            return "redirect:/shiro/login";
        }
    }

    @GetMapping("/logout")
    public String login(){
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "redirect:/shiro/login";
    }

    /**
     * @ModelAttribute msg 由redirect传入
     * @param str
     * @return
     */
    @GetMapping("/login")
    public String shiroLogin(@ModelAttribute("msg") String str){
        return "/shiro/login";
    }

    @GetMapping("/auth/{id}")
    public String auth(@PathVariable("id") Integer id)
    {
        String ret = "/auth/authPage"+id;
        return ret;
    }

    @GetMapping("/auth/annon")
    @ResponseBody
    public String annon()
    {
        return "abc";
    }

    /**
     * 未授权的页面
     * @return
     */
    @GetMapping("/no_auth")
    @ResponseBody
    public String noauth(){
        return "no Auth";
    }



    @GetMapping("/auth/data")
    @ResponseBody
    public String getData()
    {
        return "Hello data get";
    }


    @PostMapping("/auth/data")
    @ResponseBody
    public String postData()
    {
        return "Hello data post";
    }









}
