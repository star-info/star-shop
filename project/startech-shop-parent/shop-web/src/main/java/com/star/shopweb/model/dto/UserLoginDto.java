package com.star.shopweb.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-15 12:00
 */
@Data
public class UserLoginDto {
    @NotNull(message = "用户名不能为空")
    private String  username;
    @NotNull(message = "密码不能为空")
    private String  password;
}
