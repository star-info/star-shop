
package com.star.shopweb.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.star.memberapi.dto.UserAuthDTO;
import com.star.common.constants.RetCode;
import com.star.common.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.star.shopweb.base.controller.BaseController;
import com.star.shopweb.feign.UserFeign;

/**
 * 用户注册Controller
 * @author zjy
 */
@Controller
public class RegistController extends BaseController {

	private static final String REGISTER_VIEW = "locaRegist";
	private static final String LOGIN_VIEW = "login";

	@Autowired
	private UserFeign userFeign;

	@GetMapping("/locaRegist")
	public String locaRegist() {
		return REGISTER_VIEW;
	}

	@GetMapping("/regist")
	public String regist(UserAuthDTO userEntity, HttpServletRequest request) {
		try {
            Map<String, Object> registMap = userFeign.insertUser(userEntity);
            Integer code = (Integer) registMap.get(RetCode.CODE_NAME);
            if (!code.equals(RetCode.HTTP_200_CODE)) {
                String msg = (String) registMap.get("msg");
                return setError(request, msg, REGISTER_VIEW);
            }
            // 注册成功 跳转至登陆页面
            return LOGIN_VIEW;
        } catch (BusinessException be){
            return setError(request, be.getMessage(), REGISTER_VIEW);
		} catch (Exception e) {
			e.printStackTrace();
			return setError(request, "注册失败!", REGISTER_VIEW);
		}
	}

}
