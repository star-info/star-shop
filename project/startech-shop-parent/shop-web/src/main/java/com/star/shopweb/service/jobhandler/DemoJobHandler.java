package com.star.shopweb.service.jobhandler;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.IJobHandler;
import com.xxl.job.core.log.XxlJobLogger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.xxl.job.core.handler.annotation.JobHandler;
/**
 * @program: startech-shop-parent
 * @description: demo定时任务
 * @author: zjy
 * @create: 2020-07-25 19:07
 */
@JobHandler(value = "demoJobHandler")
@Service
@Slf4j
public class DemoJobHandler extends IJobHandler {

    @Value("${xxl.job.executor.port}")
    private String port;

    @Override
    public ReturnT<String> execute(String param) throws Exception {
        log.info("param {} ",param);
        XxlJobLogger.log("XXL-JOB, Hello World." + port);
        log.info("XXL-JOB, Hello World port {} {}",port,System.currentTimeMillis());
        for (int i = 0; i < 5; i++) {
            XxlJobLogger.log("beat at:" + i);
            // TimeUnit.SECONDS.sleep(2);
        }
        return ReturnT.SUCCESS;
    }
}
