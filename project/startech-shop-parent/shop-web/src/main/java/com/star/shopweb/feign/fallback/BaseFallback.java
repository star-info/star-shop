package com.star.shopweb.feign.fallback;

import com.star.common.constants.RetCode;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-22 13:51
 */
@Slf4j
public class BaseFallback {
    protected Map errorMap(){
        log.info("class {} ",this.getClass().getName());
        Map m = new HashMap();
        m.put("class",this.getClass().getName());
        m.put(RetCode.CODE_NAME,RetCode.HTTP_503_CODE);
        m.put(RetCode.MSG_NAME,"出错降级处理");
        return m;
    }
}
