package com.star.shopweb.feign.fallback;

import com.star.common.constants.RetCode;
import com.star.common.exception.BusinessException;
import com.star.memberapi.dto.UserAuthDTO;
import com.star.shopweb.base.exception.FeignException;
import com.star.shopweb.feign.FeignConfig;
import com.star.shopweb.feign.ItemFeign;
import com.star.shopweb.feign.UserFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description: ItemFeign 失败降级处理
 * @author: zjy
 * @create: 2020-07-21 19:20
 */
@Component
@Slf4j
public class UserFeignFallback extends BaseFallback implements UserFeign {

    @Override
    public Map ping() {
        throw new FeignException(FeignConfig.SERVICE_CODE_MEMBER,FeignConfig.SERVICE_MEMBER,"ping error!");
    }

    @Override
    public UserAuthDTO getUserInfoById(Integer userId) {
        throw new FeignException(FeignConfig.SERVICE_CODE_MEMBER,FeignConfig.SERVICE_MEMBER,"getUserInfoById error!");
    }

    @Override
    public Map insertUser(UserAuthDTO userAuthDto) {
        return errorMap();
    }

    @Override
    public Map login(UserAuthDTO userAuthDto) {
        Map m = errorMap();
        m.put(RetCode.MSG_NAME,"服务异常");
        return m;
    }
}
