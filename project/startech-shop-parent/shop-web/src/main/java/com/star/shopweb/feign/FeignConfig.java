package com.star.shopweb.feign;

/**
 * Feign配置
 * @author a
 */
public interface FeignConfig {
    /**
     *  接口网关服务名称 所有Feign请求先通过网关转发
     */
    String API_GATEWAY_SERVICE = "api-gateway";

    String SERVICE_MEMBER = "member";
    String SERVICE_ITEM   = "commodity";

    Integer SERVICE_CODE_MEMBER    = 1;
    Integer SERVICE_CODE_ITEM      = 2;
    Integer SERVICE_CODE_MESSAGE   = 3;


}
