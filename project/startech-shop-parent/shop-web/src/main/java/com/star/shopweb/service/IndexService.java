package com.star.shopweb.service;

import com.star.common.constants.RetCode;
import com.star.common.redis.BaseRedisService;
import com.star.memberapi.dto.UserAuthDTO;
import com.star.shopweb.feign.ItemFeign;
import com.star.shopweb.feign.UserFeign;
import com.star.shopweb.model.vo.ServiceErrorVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-22 15:30
 */
@Service
@Slf4j
public class IndexService {

    // index页面
    public static final String INDEX = "index";
    // 错误页面
    public static final String ERROR = "common/error";

    private static final String LOGIN = "login";

    @Autowired
    private ItemFeign itemFeign;
    @Autowired
    private UserFeign userFeign;

    @Autowired
    private BaseRedisService baseRedisService;

    public String goIndex(HttpServletRequest request,String token){

        String username = "";
        if(token != null){
            if(token.isEmpty()){
                // guest 游客访问
                username = "游客";
            }
            else
            {
                String userId =  (String)baseRedisService.get(token);
                if(userId != null){
                    UserAuthDTO userAuthDTO = userFeign.getUserInfoById(Integer.valueOf(userId));
                    if(userAuthDTO!=null){
                        username = userAuthDTO.getUsername();
                    }

                }
                else
                {
                    log.info("登陆过期！ token:"+token);
                    username = "游客";
                    //return LOGIN;
                }
            }
        }

        // 查询所有商品
        Map<String, Object> resultItemMap = itemFeign.getIndexItem();
        Integer code = (Integer) resultItemMap.get(RetCode.CODE_NAME);
        if (code.equals(RetCode.HTTP_200_CODE)) {
            Map<String, Object> mapItem = (Map<String, Object>) resultItemMap.get("data");
            request.setAttribute("mapItem", mapItem);
        }else{
            // 服务方返回 异常处理
            ServiceErrorVO serviceErrorVO = new ServiceErrorVO();
            serviceErrorVO.setConsumer(Thread.currentThread().getStackTrace()[1].getClassName()+"."+Thread.currentThread().getStackTrace()[1].getMethodName());
            serviceErrorVO.setProvider(ItemFeign.class.getName());
            return gotoError(request,serviceErrorVO);
        }

        request.setAttribute("username",username);
        return INDEX;
    }

    public String gotoError(HttpServletRequest request, ServiceErrorVO info){
        request.setAttribute("serviceInfo",info);
        return ERROR;
    }

    public UserAuthDTO getUserInfo(String token){
        if(!token.isEmpty()){
            String userId =  (String)baseRedisService.get(token);
            if(userId != null){
                UserAuthDTO userAuthDTO = userFeign.getUserInfoById(Integer.valueOf(userId));
                if(userAuthDTO!=null){
                    return userAuthDTO;
                }
            }
        }
        return null;
    }
}
