package com.star.shopweb.controller;

import com.star.shopweb.base.controller.BaseController;
import com.star.shopweb.feign.ItemFeign;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-20 22:10
 */
@Controller
@Slf4j
public class ItemController extends BaseController {

    private static final String ITEMDESC = "itemDesc";

    @Autowired
    private ItemFeign itemFeign;

    /**
     * 商品详情
     * @param request
     * @param id
     * @return
     */
    @GetMapping("/itemDesc")
    @ApiOperation(value="商品详情", notes="商品详情描述", produces="application/json")
    @ApiImplicitParam(name = "id", value = "商品id", paramType = "query", required = true, dataType = "Long")
    public String itemDesc(HttpServletRequest request, Long id) {
        try {
            Map<String, Object> resultItem = itemFeign.geItem(id);
            Map<String, Object> item = (Map<String, Object>) getResultMap(resultItem);
            request.setAttribute("item", item);
            Map<String, Object> reusltItemDesc = itemFeign.getItemDesc(id);
            Map<String, Object> itemDesc = (Map<String, Object>) getResultMap(reusltItemDesc);
            request.setAttribute("itemDesc", itemDesc);
            return ITEMDESC;
        } catch (Exception e) {
            log.error("###itemDesc() ERROR:", e);
            return ERROR;
        }

    }
}
