
package com.star.shopweb.controller;

import java.util.HashMap;
import java.util.Map;
import com.star.common.constants.Constants;
import com.star.shopweb.feign.ItemFeign;
import com.star.shopweb.feign.UserFeign;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.star.shopweb.base.controller.BaseController;
import lombok.extern.slf4j.Slf4j;


/**
 * Demo
 * @author zjy
 */
@Api(tags = "Demo接口")
@Slf4j
@Controller
public class DemoController  extends BaseController {

	/**
	 * 测试请求
	 * @return
	 */
	@ApiOperation(value="ping", notes="测试连接", produces="application/json")
	@ApiImplicitParam(name = "word", value = "单词", paramType = "query", required = true, dataType = "String")
	@GetMapping("/ping")
	@ResponseBody
	public Map ping(){
		Map m = new HashMap();
		m.put("name","mobile-view");
		m.put("version","1.0.1");
		m.put("info","初始");
		return m;
	}

	/**
	 * get请求测试
	 * @return
	 */
	@GetMapping("/get")
	@ResponseBody
	public Map getRequest(){
		Map m = new HashMap();
		m.put("type","get");
		m.put("data","ok");
		return m;
	}

	/**
	 * post请求测试
	 * @param data
	 * @return
	 */
	@PostMapping("/post")
	@ResponseBody
	public Map postRequest(@RequestBody Object data){
		log.info("object {} ",data);
		Map m = new HashMap(Constants.MAP_INITIAL_CAPACITY);
		m.put("type","post");
		m.put("data","ok");
		return m;
	}

	@Autowired
	UserFeign userFeign;
	@Autowired
	ItemFeign itemFeign;
	/**
	 * 服务检查
	 * @return
	 */
	@GetMapping("/check")
	@ResponseBody
	public Map serviceCheck(){
		Map m = new HashMap(Constants.MAP_INITIAL_CAPACITY);
		try
		{
			Map result = userFeign.ping();
			m.put("userFeign","ok");
			m.put("user-info",result);
		}catch (Exception e){
			e.printStackTrace();
			m.put("userFeign","error");
		}

		try
		{
			Map result = itemFeign.ping();
			m.put("itemFeign","ok");
			m.put("item-info",result);
		}catch (Exception e){
			e.printStackTrace();
			m.put("itemFeign","error");
		}
		return m;

	}

	@GetMapping("/schedule")
	@ResponseBody
	public String scheduleTest(){
		log.info("done");
		return "ok";
	}
	
}
