
package com.star.shopweb.controller;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.star.common.constants.Constants;
import com.star.common.util.CookieUtil;
import com.star.memberapi.dto.UserAuthDTO;
import cn.hutool.core.convert.Convert;
import com.star.shopweb.model.dto.UserLoginDto;
import com.star.common.constants.RetCode;
import com.star.shopweb.service.IndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.star.shopweb.base.controller.BaseController;
import com.star.shopweb.feign.UserFeign;

/**
 * 登陆控制器
 * @author zjy
 */
@Controller
@Slf4j
public class LoginConroller extends BaseController {

	private static final String LOGIN = "login";
	private static final String INDEX = "index";

	@Autowired
	private UserFeign userFeign;

	@Autowired
	private IndexService indexService;

	@GetMapping("/login")
	public String locaLogin() {
		return LOGIN;
	}

	@PostMapping("/login")
	public String login(UserLoginDto userEntity, HttpServletRequest request, HttpServletResponse response) {

		UserAuthDTO userAuthDTO = Convert.convert(UserAuthDTO.class,userEntity);
		userAuthDTO.setEmail("");

		// 调用
		Map<String, Object> login = userFeign.login(userAuthDTO);
		Integer code = (Integer) login.get(RetCode.CODE_NAME);
		if (!code.equals(RetCode.HTTP_200_CODE)) {
			String msg = (String) login.get("msg");
			return setError(request, msg, LOGIN);
		}

		try{
			// 登录成功之后,获取token.将这个token存放在cookie
			Map m = (Map) login.get("data");
			log.info("result  data {} ",m);
			String token = (String)m.get(Constants.COOKIE_TOKEN);
			CookieUtil.addCookie(response, Constants.COOKIE_TOKEN, token, Constants.WEBUSER_COOKIE_TOKEN_TERMVALIDITY);
			// 前往index页面
			return indexService.goIndex(request,token);
		}catch (NullPointerException npe){
			throw npe;
		}
	}

}
