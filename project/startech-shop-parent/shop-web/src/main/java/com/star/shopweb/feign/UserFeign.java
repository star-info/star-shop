
package com.star.shopweb.feign;

import com.star.memberapi.api.UserApi;
import com.star.shopweb.feign.fallback.UserFeignFallback;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author a
 */
@FeignClient(value=FeignConfig.API_GATEWAY_SERVICE+"/api-member",fallback= UserFeignFallback.class)
public interface UserFeign extends UserApi
{
}
