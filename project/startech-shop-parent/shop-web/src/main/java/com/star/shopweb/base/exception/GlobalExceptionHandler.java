package com.star.shopweb.base.exception;

import com.star.common.constants.Constants;
import com.star.shopweb.model.vo.ErrorInfoVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.HandlerMethod;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description: 全局异常处理
 * @author: zjy
 * @create: 2020-07-21 16:30
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    public static final String ERROR = "common/exception";

    @ExceptionHandler(FeignException.class)
    public Map feignException(HttpServletRequest request, Exception ex, HandlerMethod handlerMethod) {
        log.error("全局异常捕获 feign {}",ex.getMessage());
        FeignException fe= (FeignException)ex;

        Map result = new HashMap(Constants.MAP_INITIAL_CAPACITY);
        result.put(fe.getName(),ex.getMessage());
        return result;
    }

    @ExceptionHandler(Exception.class)
    public String customException(HttpServletRequest request, Exception ex, HandlerMethod handlerMethod) {


        log.error("全局异常捕获 Exception {}",ex.getClass().getName());

        String exMessage = ex.getMessage();
        // 空指针异常的报错信息是null
        if(exMessage == null){
            exMessage = "Null Pointer Exception";
        }

        String exMethodName = handlerMethod.getMethod().getName();
        Class exClass = handlerMethod.getBean().getClass();
        String exClassName = exClass.getName();
        String error = "异常信息: "+ exMessage + " 异常方法: " + exMethodName + " 异常类: " + exClassName;
        log.error(error);

        ErrorInfoVO vo = new ErrorInfoVO();
        vo.setMethodName(exMethodName);
        vo.setMsg(exMessage);
        vo.setExClass(exClassName);
        vo.setStackTrace(ex.getStackTrace());

        request.setAttribute("errorInfo",vo);

        // 返回错误页面
        return ERROR;
    }
}
