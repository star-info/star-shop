package com.star.shopweb.controller;

import com.star.common.constants.Constants;
import com.star.common.util.CookieUtil;
import com.star.memberapi.dto.UserAuthDTO;
import com.star.shopweb.base.controller.BaseController;
import com.star.shopweb.feign.ItemFeign;
import com.star.shopweb.service.IndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: startech-shop-parent
 * @description: 所有控制视图跳转
 * @author: zjy
 * @create: 2020-07-20 15:32
 */
@Controller
@Slf4j
public class ViewController extends BaseController {

    private static final String LOGIN = "login";

    public static final String USER_INFO = "/user/userInfo";

    @Autowired
    private IndexService indexService;

    @GetMapping("/index")
    public String index(HttpServletRequest request) {
        String value = CookieUtil.getUid(request, Constants.COOKIE_TOKEN);
        return indexService.goIndex(request,value);
    }

    /**
     * 用户个人中心页面
     * @param request
     * @return
     */
    @GetMapping("/user_info")
    public String gotoUserInfo(HttpServletRequest request) {
        String token = CookieUtil.getUid(request, Constants.COOKIE_TOKEN);
        if(token.isEmpty()){
            // guest 游客
            log.info("游客访问，请登陆！");
            return LOGIN;
        }

        // 获取用户信息 从redis中查询用户Id
        UserAuthDTO userAuthDTO = indexService.getUserInfo(token);
        // 调用会员服务 查询出用户信息
        if(userAuthDTO == null){
            log.info("token过期！，请登陆！");
            return LOGIN;
        }

        log.info("userinfo is {} "+userAuthDTO);
        request.setAttribute("user_info",userAuthDTO);
        return USER_INFO;
    }


}
