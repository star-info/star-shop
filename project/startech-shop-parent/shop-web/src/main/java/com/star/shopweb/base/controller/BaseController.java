
package com.star.shopweb.base.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.star.common.constants.RetCode;
import com.star.shopweb.model.vo.ServiceErrorVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.star.shopweb.feign.UserFeign;

/**
 * 
 * @classDesc: 功能描述:()
 * @author: zjy
 * @QQ:
 * @createTime: 2017年10月26日 下午10:50:39
 * @version: v1.0
 * @copyright:每特学院(蚂蚁课堂)上海每特教育科技有限公司
 */
@Controller
public class BaseController {

	// 错误页面
	public static final String ERROR = "common/error";

	@Autowired
	private UserFeign userFeign;

	public static Object getResultMap(Map<String, Object> reusltItemDesc) {
		Integer code = (Integer) reusltItemDesc.get(RetCode.CODE_NAME);
		if (code.equals(RetCode.HTTP_200_CODE)) {
			Object object = reusltItemDesc.get("data");
			return object;
		}
		return null;
	}

	public String gotoError(HttpServletRequest request, ServiceErrorVO info){
		request.setAttribute("serviceInfo",info);
		return ERROR;
	}

//	public UserEntity getUserEntity(String token) {
//		Map<String, Object> userMap = userFeign.getUser(token);
//		Integer code = (Integer) userMap.get(BaseApiConstants.HTTP_CODE_NAME);
//		if (!code.equals(BaseApiConstants.HTTP_200_CODE)) {
//			return null;
//		}
//		// 获取data参数
//		LinkedHashMap linkedHashMap = (LinkedHashMap) userMap.get(BaseApiConstants.HTTP_DATA_NAME);
//		String json = new JSONObject().toJSONString(linkedHashMap);
//		UserEntity userEntity = new JSONObject().parseObject(json, UserEntity.class);
//		return userEntity;
//
//	}

	public String setError(HttpServletRequest request, String msg, String addres) {
		request.setAttribute("error", msg);
		return addres;
	}

}
