package com.star.shopweb.feign.fallback;

import com.star.common.constants.RetCode;
import com.star.itemapi.api.ItemService;
import com.star.shopweb.base.exception.FeignException;
import com.star.shopweb.feign.FeignConfig;
import com.star.shopweb.feign.ItemFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description: ItemFeign 失败降级处理
 * @author: zjy
 * @create: 2020-07-21 19:20
 */
@Component
@Slf4j
public class ItemFeignFallback extends BaseFallback implements ItemFeign {

    @Override
    public Map ping() {
        throw new FeignException(FeignConfig.SERVICE_CODE_ITEM,FeignConfig.SERVICE_ITEM,"ping error!");
    }

    @Override
    public Map<String, Object> getIndexItem() {
        throw new FeignException(FeignConfig.SERVICE_CODE_ITEM,FeignConfig.SERVICE_ITEM,"getIndexItem error!");
    }

    @Override
    public Map<String, Object> geItem(Long id) {
        return errorMap();
    }

    @Override
    public Map<String, Object> getItemDesc(Long id) {
        return errorMap();
    }
}
