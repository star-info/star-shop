package com.star.shopweb.feign;

import com.star.itemapi.api.ItemService;
import com.star.shopweb.feign.fallback.ItemFeignFallback;
import org.springframework.cloud.netflix.feign.FeignClient;

/**
 * @author a
 */
@FeignClient(value=FeignConfig.API_GATEWAY_SERVICE+"/api-item",fallback=ItemFeignFallback.class)
public interface ItemFeign extends ItemService {
}