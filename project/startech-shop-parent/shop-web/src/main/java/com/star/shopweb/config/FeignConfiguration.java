package com.star.shopweb.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;

/**
 * @program: startech-shop-parent
 * @description: Feign拦截器
 * @author: zjy
 * @create: 2020-07-24 21:29
 */
@Configuration
public class FeignConfiguration implements RequestInterceptor {
    /**
     * 目的 请求网关在头部告诉自己来源 这里暂时使用这种方案
     * @param requestTemplate
     */
    @Override
    public void apply(RequestTemplate requestTemplate) {;
        requestTemplate.header("name", "shop-web");
        requestTemplate.header("from", "gateway");
    }
}
