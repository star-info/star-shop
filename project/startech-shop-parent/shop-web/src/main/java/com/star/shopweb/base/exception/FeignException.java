package com.star.shopweb.base.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 微服务异常
 * @Author: zjy
 * @Date: 2020/7/24
 */
@Setter
@Getter
public class FeignException extends RuntimeException {

	private Integer type;
	private String  name;

	public FeignException(Integer type,String name,String message) {
		super(message);
		this.type = type;
		this.name = name;
	}

	public FeignException(String message) {
		super(message);
	}

	public FeignException(String message, Throwable cause) {
		super(message, cause);
	}

	public FeignException(Throwable cause) {
		super(cause);
	}

	protected FeignException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
