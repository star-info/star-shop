package com.star.shopweb.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: startech-shop-parent
 * @description: Feign日志配置
 * @author: zjy
 * @create: 2020-07-23 14:08
 */
@Configuration
public class FeignLogConfiguration {
    @Bean
    Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }
}
