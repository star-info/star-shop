package com.star.shopweb.model.vo;

import lombok.Data;

/**
 * @program: startech-shop-parent
 * @description: 异常信息
 * @author: zjy
 * @create: 2020-07-21 18:05
 */
@Data
public class ServiceErrorVO {
    private String consumer;
    private String provider;

}
