<html xmlns:th="http://www.thymeleaf.org" >
<head>
<script src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript">
    $(function() {
        console.log("===================");
        let aa = '${errorInfo}';
        //var courseDto = '<%=request.getAttribute("errorInfo")%>'
        var errorInfo = "${errorInfo}";
        console.log(errorInfo);

    });
</script>
</head>
<body>
<h2>系统错误</h2>
<div>
    <p>异常类  : ${errorInfo.exClass}</p>
    <p>异常方法 : ${errorInfo.methodName}</p>
    <p>异常信息 : ${errorInfo.msg}</p>
    <p>堆栈信息 </p>
    <#list errorInfo.stackTrace as su>
<#--        console.log("${su}");-->
        <span >${su}</span>
        <br/>
    </#list>
<#--    <div th:each="line:${errorInfo.stackTrace}">-->
<#--        <div th:text="   ${line}"></div>-->
<#--    </div>-->
</div>
</body>
</html>