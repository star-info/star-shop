package com.star.common.util;

import cn.hutool.core.convert.Convert;
import com.google.common.collect.Lists;


import java.util.List;


/**
 * @author yyz
 */
public class ConvertUtil {

    public static <T, DTO> List<DTO> dtoConvert(List<T> result, Class<DTO> dtoClass) {
        //数据类型转换
        List<DTO> content = Lists.newArrayList();
        result.stream().forEach(
                dto -> {
                    DTO data = Convert.convert(dtoClass, dto);
                    content.add(data);
                }
        );
        return content;
    }


    public static <T, DTO> DTO dtoConvert(T result, Class<DTO> dtoClass) {
        return Convert.convert(dtoClass, result);
    }

}
