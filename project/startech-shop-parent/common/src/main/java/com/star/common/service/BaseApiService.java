package com.star.common.service;

import com.star.common.constants.RetCode;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-08 23:44
 */
public class BaseApiService {

    public Map<String,Object> setResultError(String msg){
        return setResult(RetCode.HTTP_500_CODE,msg,null);
    }

    public Map<String,Object> setResultOk(){
        return setResultOk(null);
    }

    public Map<String,Object> setResultOk(Object data){
        return setResult(RetCode.HTTP_200_CODE,"ok",data);
    }

    public Map<String,Object> setResult(Integer code,String msg,Object data){
        Map map = new HashMap<>();
        map.put(RetCode.CODE_NAME,code);
        map.put(RetCode.MSG_NAME,msg);
        if(data != null){
            map.put("data",data);
        }
        return map;
    }
}
