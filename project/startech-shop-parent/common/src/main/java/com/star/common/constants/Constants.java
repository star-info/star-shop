package com.star.common.constants;

public interface Constants {

    Integer MAP_INITIAL_CAPACITY = 16;

    // redis 时长 24小时
    Long USER_TOKEN_TEAMVALIDITY = 60 * 60 * 24L;
    // cookie 有效期 24小时
    Integer WEBUSER_COOKIE_TOKEN_TERMVALIDITY = 1000 * 60 * 60 * 24;

    // token 名称
    String COOKIE_TOKEN = "token";
}
