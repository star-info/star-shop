package com.star.common.util;


import java.util.Stack;

public class SkuUtil {
    private static char[] charSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".toCharArray();
    private static char[] char36Set = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();


    public static String _10To62(long number) {
        Long rest = number;
        Stack<Character> stack = new Stack<Character>();
        StringBuilder result = new StringBuilder(0);
        while (rest != 0) {
            stack.add(charSet[new Long((rest - (rest / 62) * 62)).intValue()]);
            rest = rest / 62;
        }
        for (; !stack.isEmpty(); ) {
            result.append(stack.pop());
        }

        return result.toString();

    }

    public static String _10To36(long number) {
        Long rest = number;
        Stack<Character> stack = new Stack<Character>();
        StringBuilder result = new StringBuilder(0);
        while (rest != 0) {
            stack.add(char36Set[new Long((rest - (rest / 36) * 36)).intValue()]);
            rest = rest / 36;
        }
        for (; !stack.isEmpty(); ) {
            result.append(stack.pop());
        }

        return result.toString();

    }


}
