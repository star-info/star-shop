package com.star.common.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-26 02:01
 */
@Configuration
@EnableSwagger2
public  class Swagger2Config {

    // TODO 这里配置报错
    //api接口包扫描路径
    //public static final String SWAGGER_SCAN_BASE_PACKAGE = "com.star.controller";
    public static final String SWAGGER_SCAN_BASE_PACKAGE = "com.star.shopweb.controller";
    protected static final String VERSION = "1.0.0";

    @Bean
    protected Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(SWAGGER_SCAN_BASE_PACKAGE))
                //.apis(RequestHandlerSelectors.withClassAnnotation(ApiOperation.class))
                .paths(PathSelectors.any()) // 可以根据url路径设置哪些请求加入文档，忽略哪些请求
                .build();
    }

    protected ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("star-shop API 接口文档") //设置文档的标题
                .description("描述") // 设置文档的描述
                .version(VERSION) // 设置文档的版本信息-> 1.0.0 Version information
                .termsOfServiceUrl("https://www.yuque.com/u1644073") // 设置文档的License信息->1.3 License information
                .build();
    }
}
