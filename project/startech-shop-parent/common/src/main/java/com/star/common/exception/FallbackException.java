package com.star.common.exception;

/**
 * @Author: yeze_yang
 * @Date: 2019/1/4/004 17:51
 */
public class FallbackException extends RuntimeException {
	public FallbackException() {
		super();
	}

	public FallbackException(String message) {
		super(message);
	}

	public FallbackException(String message, Throwable cause) {
		super(message, cause);
	}

	public FallbackException(Throwable cause) {
		super(cause);
	}

	protected FallbackException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
