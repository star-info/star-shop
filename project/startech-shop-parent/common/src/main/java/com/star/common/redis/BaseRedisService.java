package com.star.common.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * Redis 操作封装
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-09 10:53
 */
@Component
public class BaseRedisService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedisTemplate redisTemplate;

    private static final long DEFAULT_EXPIRE = 300L;

    public void selectDB(int index){
        JedisConnectionFactory connectionFactory =
                (JedisConnectionFactory) redisTemplate.getConnectionFactory();
        connectionFactory.setDatabase(index);
    }

    public long getDefaultExpire(){
        return DEFAULT_EXPIRE;
    }

    public void setString(String key,String value){
        // 默认时间300秒 即5分钟
        set(key,value,DEFAULT_EXPIRE);
    }

    public void setObject(String key,Object value) {
        redisTemplate.opsForValue().set(key, value);
        redisTemplate.expire(key,DEFAULT_EXPIRE, TimeUnit.SECONDS);
    }
    // 刷新时间
    public void keepStringAlive(String key){
        stringRedisTemplate.expire(key,DEFAULT_EXPIRE, TimeUnit.SECONDS);
    }

    /**
     * 添加
     * @param key
     * @param value
     * @param timeOut
     */
    public void set(String key,Object value,Long timeOut){
        if(value != null){
            if(value instanceof String){
                String setValue = (String)value;
                stringRedisTemplate.opsForValue().set(key,setValue);
            }
            else{
                System.out.println(" value is not string");
            }

            if(timeOut !=null){
                stringRedisTemplate.expire(key,timeOut, TimeUnit.SECONDS);
            }
        }
    }

    /**
     * 查找
     * @param key
     * @return
     */
    public Object get(String key){
        return stringRedisTemplate.opsForValue().get(key);
    }

    /**
     * 删除
     * @param key
     */
    public void delete(String key){
        stringRedisTemplate.delete(key);
    }



}
