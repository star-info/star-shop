package com.star.common.constants;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-08 23:57
 */
public interface RetCode {
    /**
     * 成功
     */
    Integer HTTP_200_CODE=200;

    /**
     * 服务提供方 出错
     */
    Integer HTTP_500_CODE=500;

    String  CODE_NAME="code";
    String  MSG_NAME="msg";

    /**
     * 服务降级
     */
    Integer HTTP_503_CODE=503;



}
