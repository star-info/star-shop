package com.star.common.entity;


import lombok.Data;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 10:50
 */
@Data
public class BaseEntity {
    protected Long id;

    //private Long updated_time;
}
