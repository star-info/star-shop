package com.star.common.util;

import lombok.extern.slf4j.Slf4j;

/**
 * @program: startech-shop-parent
 * @description: 系统工具
 * @author: zjy
 * @create: 2020-07-10 08:41
 */
@Slf4j
public class SystemUtil {
    public static void main(String[] args) {
        log.info("CPU 核心数 {} ",Runtime.getRuntime().availableProcessors());
        log.info("CPU 密集 推荐线程数 {}",getRecommandMaxThreadNumCPUBound());
        log.info("IO 密集 推荐线程数 {}",getRecommandMaxThreadNumIOBound());
    }

    /**
     * 获取CPU 核心数
     * @return
     */
    public static int getCpuProcessors(){
        return Runtime.getRuntime().availableProcessors();
    }

    /**
     * CPU 密集型 推荐的线程数
     * @return
     */
    public static int getRecommandMaxThreadNumCPUBound(){
        return getCpuProcessors()+1;
    }
    /**
     * IO 密集型 推荐的线程数
     * @return
     */
    public static int getRecommandMaxThreadNumIOBound(){
        return getCpuProcessors()*2+1;
    }

}

