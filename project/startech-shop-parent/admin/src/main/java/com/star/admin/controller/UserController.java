package com.star.admin.controller;

import com.star.common.constants.Constants;
import com.star.common.service.BaseApiService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-08 19:06
 */
@RestController
@RequestMapping("/api/user")
@Slf4j
public class UserController extends BaseApiService {

    @GetMapping("/ping")
    public Map ping(){
        Map m = new HashMap(Constants.MAP_INITIAL_CAPACITY);
        m.put("name","admin");
        m.put("version","1.0.2");
        m.put("status","ok");
        return m;
    }



}
