
package com.star.admin.controller;

import com.star.common.constants.Constants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;


/**
 * Demo
 * @author zjy
 */
@Api(tags = "Demo接口")
@Slf4j
@Controller
public class DemoController  {

	/**
	 * 测试请求
	 * @return
	 */
	@ApiOperation(value="ping", notes="测试连接", produces="application/json")
	@ApiImplicitParam(name = "word", value = "单词", paramType = "query", required = true, dataType = "String")
	@GetMapping("/ping")
	@ResponseBody
	public Map ping(){
		Map m = new HashMap();
		m.put("name","mobile-view");
		m.put("version","1.0.1");
		m.put("info","初始");
		return m;
	}

	/**
	 * get请求测试
	 * @return
	 */
	@GetMapping("/get")
	@ResponseBody
	public Map getRequest(){
		Map m = new HashMap();
		m.put("type","get");
		m.put("data","ok");
		return m;
	}

	/**
	 * post请求测试
	 * @param data
	 * @return
	 */
	@PostMapping("/post")
	@ResponseBody
	public Map postRequest(@RequestBody Object data){
		log.info("object {} ",data);
		Map m = new HashMap(Constants.MAP_INITIAL_CAPACITY);
		m.put("type","post");
		m.put("data","ok");
		return m;
	}


	
}
