package com.star.admin.entity.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.star.common.entity.BasePO;
import lombok.Data;

import java.sql.Timestamp;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 09:29
 */
@Data
@TableName("userauth")
public class UserAuth extends BasePO {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    private String  username;
    private String  password;
    private Timestamp createdTime;

    private String  email;
    private String  mobile;
}
