package com.star.admin.service;

import com.star.admin.mapper.UserAuthlMapper;
import com.star.admin.mapper.UserDetailMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-08-13 01:41
 */
@Service
@Slf4j
public class UserService {

    @Autowired
    UserDetailMapper userDetailMapper;

    @Autowired
    UserAuthlMapper userAuthlMapper;

    public List userList(){
        log.info("userDetail  {} ",userDetailMapper.selectList(null));
        List list = null;
        list = userAuthlMapper.selectList(null);
        log.info("userauth list {} ",userAuthlMapper.selectList(null));
        return list;
    }

}
