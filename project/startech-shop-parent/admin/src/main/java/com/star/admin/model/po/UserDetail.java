package com.star.admin.model.po;

import com.star.common.entity.BasePO;
import lombok.Data;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-08-13 01:17
 */
@Data
public class UserDetail extends BasePO {
    Long userId;
    String nickName;
    String address;
}
