package com.star.admin.model.bo;

import lombok.Data;

/**
 * @program: startech-shop-parent
 * @description: 登陆结果处理
 * @author: zjy
 * @create: 2020-07-15 13:30
 */
@Data
public class LoginResultBo {
    private boolean loginSuccess;
    private String msg;
    private String token;
}
