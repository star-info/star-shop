package com.star.admin.constants;

/**
 * Shiro配置的一些常量定义
 * @author zjy
 */
public interface ShiroConstants {
    /**
     * 密码加密算法
     */
    String  HASH_ALGORITHM_NAME = "MD5";
    /**
     * 密码加密次数
     */
    Integer HASH_ITERATIONS = 1024;

}
