package com.star.admin.model.vo;

import lombok.Data;

import java.util.List;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-08-13 05:25
 */
@Data
public class UserListVO {
    List list;
    Integer count;
}
