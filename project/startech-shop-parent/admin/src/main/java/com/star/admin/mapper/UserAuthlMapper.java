package com.star.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.admin.entity.po.UserAuth;
import com.star.admin.model.po.UserDetail;


/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 09:17
 */
public interface UserAuthlMapper extends BaseMapper<UserAuth> {
}
