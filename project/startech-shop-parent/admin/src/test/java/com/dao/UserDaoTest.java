package com.dao;

import com.BaseApplicationTest;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.star.admin.mapper.UserAuthlMapper;
import com.star.admin.mapper.UserDetailMapper;
import com.star.admin.model.po.UserDetail;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-10 09:42
 */
@Slf4j
public class UserDaoTest
         extends BaseApplicationTest
{
    @Autowired
    UserDetailMapper userDetailMapper;

    @Autowired
    UserAuthlMapper userAuthlMapper;

    @Test
    public void userMapperTest(){

        log.info("userDetail  {} ",userDetailMapper.selectList(null));

        log.info("userauth list {} ",userAuthlMapper.selectList(null));

    }

//    @Test
//    public void selectOneTest(){
//        UserAuth userAuth = userMapper.selectOne(Wrappers.<UserAuth>lambdaQuery().eq(UserAuth::getId,31));
//        log.info("result {} ",userAuth);
//    }

}
