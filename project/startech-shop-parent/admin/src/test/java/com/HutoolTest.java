package com;

import cn.hutool.crypto.SecureUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * @program: startech-shop-parent
 * @description:
 * @author: zjy
 * @create: 2020-07-11 11:45
 */
@Slf4j
public class HutoolTest {

    /**
     * md5 demo
     */
    @Test
    public void md5Test(){
        String password = "123";
        String md5Str = SecureUtil.md5(password);
        log.info("md5Str is {} ",md5Str);
    }

}
