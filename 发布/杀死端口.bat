cls
@echo.
@echo.
@echo                         ------根据端口号杀死进程进程工具v0.2.3（按回车继续/q退出）------
@echo.
@echo.
@echo.
@echo.

:_find
    @echo off
    set "port=0"
    set /p port=请输入被占用的端口号:
    if "%port%"=="0" goto _find
    if "%port%"=="q" goto end
    netstat -ano | findstr "%port%" >nul && Echo 以下进程占用了%port%端口: && echo   协议	 本地IP:端口		远程IP:端口		监听状态	PID && netstat -ano | findstr "%port%" || Echo %port%端口没有被任何进程占用. && goto _find

:_kill
    set "pid=0"
    set /p pid=请输入进程PID将其杀死：
    if "%pid%"=="0" goto _kill
    if "%pid%"=="q" goto end
    taskkill -PID "%pid%" -F
pause
:end
cls
