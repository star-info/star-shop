pid=$(ps -ef | grep "bus-server.jar" | grep -v grep | awk '{print $2}')
echo "bus-server.jar pid=$pid will kill"
kill -9 $pid