pid=$(ps -ef | grep "bus-server.jar" | grep -v grep | awk '{print $2}')
echo "bus-server.jar pid=$pid will kill"
kill -9 $pid
java -server -Xmx1024m -Xms2048m -Xss256k -jar bus-server.jar
